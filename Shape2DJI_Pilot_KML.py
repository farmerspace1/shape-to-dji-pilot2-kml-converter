# for first time use:
# pip install geopandas simplekml

import os
import sys
import geopandas as gpd
import simplekml
from datetime import datetime

INPUT_PATH = os.getcwd()  # input path to all files



# =============================================================================
#                      OPTIONAL USER VARIABLES
# =============================================================================

OUTPUT_PATH = INPUT_PATH
SCANDIRECTORY_AND_SUBDIRECTORIES = 'no'  # no or yes
EXTENSION_FILTRE = '.shp'  # search for all shape files
STR_FILE_FILTER=''
NAME = 'id' 

# =============================================================================
#                         FUNCTIONS
# =============================================================================

print('This skript was proudly cobbled together from other peoples mental property')
def create_directory(path: str):
    """Create a directory. Will pass if directory has been added
      by another tread."""
    # err = ''
    if os.path.isdir(path):
        pass
    else:
        try:
            os.makedirs(path)
        except WindowsError as err:
            pass
            return err


def scan_directory(path: list):
    file_lst = []
    for (directory, sub_directory, file_name) in os.walk(path):
        for files in file_name:
            if files.lower().endswith(EXTENSION_FILTRE) and STR_FILE_FILTER.lower() in files.lower():# and files.lower().startswith("variable")
                file_filter = os.path.join(directory, files)
                file_lst.append(file_filter)
        if SCANDIRECTORY_AND_SUBDIRECTORIES == 'no':
            break  # this break will exit the loop and limite to 1 directory
        elif SCANDIRECTORY_AND_SUBDIRECTORIES == 'yes':
            pass
    return file_lst


def read_input_file(file: str):
    '''
    Parameters
    ----------
    file : file name str

    Returns
    -------
    None.

    '''
    input_file = gpd.read_file(file)
    fn = os.path.basename(os.path.splitext(file)[0])
    # Check CRS and reproject if needed
    crs = input_file.crs
    if crs is None:
        print(os.path.basename(file)+' has no CRS. Output .kml will not be'
              ' well georeferenced if file is not already in EPSG 4326.')
    elif crs == 'epsg:4326':
        # print(crs)
        pass
    else:
        print(os.path.basename(file) + ' has been reprojected to EPSG 4326.')
        input_file = input_file.to_crs(4326)


    # Check geometries for Polygon
    geom_type = ''
    for q in input_file.geometry:
        if q.type == 'Polygon':
            geom_type = 'Polygon'
        else:
            message = os.path.basename(file) + ' contains objects other then Polygons '\
                     ' Please remove file from input directory'\
                     ' or modify file objects.  Note that MultiPolygon' \
                     ' and MultiLineStrings are not supported.'
            sys.exit(message)

    if geom_type == 'Polygon':
        for index, row in input_file.iterrows():
            geom = (row.geometry)
            ext = list(geom.exterior.coords)
            int_ring = []
            for interior in geom.interiors:
                int_ring.append(list(interior.coords))
            kml = simplekml.Kml()
            pg = kml.newpolygon(name=(NAME))
            pg.outerboundaryis = ext
            if int_ring == []:
                pass
            else:
                pg.innerboundaryis = int_ring
            kml.save(os.path.join(OUTPUT_PATH, fn + '_' + str( datetime.today().strftime('%Y-%m-%d')) + '.kml'))

  
    else:
        print('Only polygon files are support for now!')

    return os.path.basename(file), '--> .kml'


if __name__ == '__main__':

    files = (scan_directory(INPUT_PATH))
    create_directory(OUTPUT_PATH)
    print('The following files where found in the input directory:')
    for q in files:
        print(os.path.basename(q))
    print('\n')

    print('Translating file geometries to kml...', '\n')

    for message in (map(read_input_file, files)):
        print(message[0], message[1])

    print('Done')




