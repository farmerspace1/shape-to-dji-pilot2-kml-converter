How to use the shp to kml converter:
install Python, I used anaconda, pycharm works as well, other versions not tested but should work
setup:
put .bat file in a folder with pythonscript and shp file (polygon) 

go to python terminal and pip install dependencies:
pip install simplekml geopandas

optional:
	test skript once in enviroment like spyder

edit bat file to redirect to local python installation


run bat file
if it works edit batfile and remove "PAUSE" at the end
enjoy


The code is blatantly adapted from:
# Shape2DJI_Pilot_KML.py
https://gitlab.com/njacadieux/shape2dji_pilot_kml.py

This python script will scan a directory, find all the .shp files, reproject to EPSG 4326, create an output directory and make a new .kml for every line or polygon found in the files.  .KML are 100% compatible with DJI PILOT.  This is not the case with .kml files created by ESRI or QGIS.


- Nicolas Cadieux, McGill Applied Remote Sensing Lab, https://arsl.geog.mcgill.ca/.
- YouTube video with instructions https://youtu.be/Wzr-zP5wG1g
